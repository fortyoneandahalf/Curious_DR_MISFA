 
__author__ = 'Varun Kompella, varun@idsia.ch'


"""
Least Square Policy Iteration RL node

"""

import numpy as np
from scipy.linalg import pinv, norm
import time
from basisFn import basisFnNode
from inspect import isfunction, ismethod

class LSPIModelNode(object):
    def __init__(self, numActions, stateLims, basis, gamma, **kwargs):
        '''
        Inputs:
        
        numActions : Number of discrete actions
        stateLims  : State Limits
        basis      : Type of Basis Function 
        gamma      : Discount factor 
        
        Optional Args:
        R          : World Reward Model - either as a function or a matrix/vector, None (Default)
        
        '''
        self.numActions = numActions                    # Number of discrete actions
        self.stateLims = np.asarray(stateLims)          # State Limits
        self.basis = basis                              # Type of Basis Function 
        self.gamma = gamma                              # Discount factor
        self.kwargs = kwargs                            # Optional arguments
        
        self._R = self.kwargs.get('R', None)            # World Reward Model

        self.dim = len(stateLims)                       # Dimension of the state space
        self.basisfn = basisFnNode(self.basis, self.dim, self.stateLims, **kwargs)   # Basis Functions

        self.numFeatures = self.basisfn.numBasisFns     # Number of stateFeatures
        self.k = self.numActions*self.numFeatures       # Number of basis functions (State-action features)
        self.theta = np.zeros([1, self.k])              # Initial weight vector for Q values
        self.eps = self.kwargs.get('epsilon', 0.005)    # Stopping criterion

        self._A = np.zeros([self.k,self.k])
        self._b = np.zeros([1,self.k])

        self.D = self.DIndx = None
        self.times = {'PI':0, 'PE':0}                   # Execution times
        self.error = 0.0
        self.errorHist = []                             # Error History


    def P(self, s, a, s_, **kwargs):
        return self.basisfn.gp.getTransProb(s, a, s_, **kwargs)
        

    def R(self, s, a, s_, **kwargs):
        if isfunction(self._R) or ismethod(self._R):
            return self._R(s,a,s_,**kwargs)
        else:
            if len(self._R.shape) == 1:
                return self._R[s_]
            elif len(self._R.shape) == 2:
                return self._R[s,a]
            elif len(self._R.shape) == 3:
                return self._R[s,a,s_]
            else:
                raise Exception('R must be a state vector or a state-action matrix!')


    def genSimulatedSamples(self, b=None):
        P = self.P
        self.D = []; self.DIndx = [] 
        for i in xrange(self.basisfn.gp.totGraphNodes):
            I_ = self.basisfn.gp.getValidNeighbors(i)
            A = list(set(self.basisfn.gp.getValidTransitions(i)))
            s = self.basisfn.gp.graphIndx2GridX(i)
            S_ = self.basisfn.gp.graphIndx2GridX(I_)
            for a in A:
                m = filter(lambda x: P(i,a,I_[x]) > 0, xrange(len(I_)))
                Sm_ = S_[list(m)]
                Im_ = I_[list(m)]
                self.D.append([s.ravel(), a, Sm_.T])
                self.DIndx.append([i, a, Im_.T])
        self.updateInternals()
            
    def updateInternals(self):
        if (self.D is not None) or (self.DIndx is not None):
            self._updatePhi()
            self._updateqValsPi()


    def _updatePhi(self):
        self._Phi = np.zeros([self.basisfn.gp.totGraphNodes, self.numActions, 1, self.k])
        _ind = np.asarray([x[:2] for x in self.DIndx], dtype='int')
        I = _ind[:,0:1]; A = _ind[:,1:2]
        S = np.asarray([x[0] for x in self.D])
        phi_s = self.basisfn(I)

        for i in xrange(len(S)):
            phi = np.zeros([self.numActions, self.numFeatures])
            phi[A[i]] = phi_s[i]
            self._Phi[I[i], A[i]] = phi.reshape(1, self.numActions*self.numFeatures)

    def _updateqValsPi(self):
        self._Pi = np.zeros(self.basisfn.gp.totGraphNodes)
        self._qVals = np.zeros([self.basisfn.gp.totGraphNodes, self.numActions])
        for m in xrange(len(self.DIndx)):
            i = self.DIndx[m][0]
            self._qVals[i] = np.asarray([np.dot(self.phi(i,a), self.theta.T) for a in xrange(self.numActions)]).ravel()
            A = np.asarray(self.basisfn.gp.getValidTransitions(i))
            if (self._qVals[i,:] == self._qVals[i,0]).all():
                self._Pi[i] = A[np.random.randint(0,len(A))]
            else:
                qvals = np.asarray(self._qVals[i])
                self._Pi[i] = A[np.argmax(qvals[A])]

    def phi(self, s, a=None):
        return self._Phi[s,int(a)]
            
    def qValues(self, s):
        return self._qVals[s]

    def pi(self, s):
        return self._Pi[s]
            

    def _LSTDQUpdate(self, D):
        self._A = np.zeros([self.k,self.k])
        self._b = np.zeros([1,self.k])
        for sample in D:
            [s, a, S_] = sample
            phi_n = np.zeros([1,self.k])
            r_n = 0.0
            for s_ in S_:
                P = self.P(s,a,s_)
                if P==0:
                    continue
                phi_n += P*self.phi(s_,self.pi(s_))
                r_n += P*self.R(s,a,s_)
            phi = self.phi(s,a)
            self._A += np.dot(phi.T, (phi - self.gamma*phi_n))
            self._b += phi*r_n

        theta =  np.dot(self._b,pinv(self._A.T))
        return theta

    def update(self, iterval=np.inf, verbose=False):
        self.genSimulatedSamples()
        D = self.DIndx

        t = time.time()
        self.error = np.inf
        cnt = 0.
        while (self.error > self.eps):
            self._theta = self.theta.copy()
            t1 = time.time()
            self.theta = self._LSTDQUpdate(D)
            self.times['PE']+= time.time()-t1
            self.error = norm(self.theta - self._theta)
            self.errorHist.append(self.error)
            self._updateqValsPi()
            if verbose:
                print 'theta error: ',self.error
            cnt+=1.
            if cnt >= iterval:
                break
        self.times['PI'] = time.time()-t
        self.times['AvgPE'] = self.times['PE']/cnt
        return self.theta

            
    def reset(self):
        self.k = self.numActions*self.numFeatures       # Number of basis functions (State-action features)
        self.theta = np.zeros([1, self.k])              # Initial weight vector for Q values


