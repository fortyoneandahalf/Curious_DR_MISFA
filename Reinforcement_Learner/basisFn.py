
__author__ = 'Varun Kompella, varun@idsia.ch'


"""
Basis Functions Node

"""

import numpy as np
import itertools

        
class basisFnNode(object):
    '''
    Basis Functions Node
    
    Input Args:
    
    basistype        : Basis function type. (Current: only Indicator)
    dim              : Dimension
    lims             : State lims
    
    
    '''
    
    def __init__(self, basistype, dim, lims, **kwargs):
        self.basistype = basistype
        self.dim = dim
        self.lims = np.atleast_2d(lims).astype('float')
        assert (len(lims) == dim)
        self.kwargs = kwargs
        
        self.gp = gridProcessingNode(gridLims=self.lims)
        self.gp.updateAdjMatrix(**kwargs)

        if self.basistype is 'Indicator':
            self._fn = self._indicator
            self.numBasisFns = np.prod(self.gp.numGridNodes)
            
        else:
            raise Exception('Unrecognized basisfun type')


    ###Basis Function Calls############################################################

    def _indicator(self, x, **kwargs):
        phi = np.zeros([x.shape[0],self.numBasisFns])
        for i in xrange(x.shape[0]):
            phi[i,x[i:i+1]] = 1.
        return phi

    ###############################################################

    def __call__(self, x, *args, **kwargs):
        x = np.atleast_2d(x)
        return self._fn(x, *args, **kwargs)
    


class gridProcessingNode(object):
    '''
    Grid/Graph Nodes Processing
    
    Input Args:
    
    gridLims         : [[a1,b1],[a2,b2],...] Grid limits for each dimension
    numGridNodes     : [m1, m2, ...] Number of nodes for each dimension
    gridRadius       : nearest neighbor radius
    order              : distance type Lp lesbegue measure.
    centralElem      : include central element or not
    
    '''
    def __init__(self, gridLims, numGridNodes=None, gridRadius=1, order=1, centralElem=True):
        self.gridLims = np.atleast_2d(gridLims)
        if numGridNodes is None:
            self.numGridNodes = (self.gridLims[:,1]-self.gridLims[:,0] + 1).astype('int')
        else:
            self.numGridNodes = np.asarray(numGridNodes)
        self.gridRadius = gridRadius
        self.order = order
        self.centralElem = centralElem
        assert(self.gridLims.shape[0] == self.numGridNodes.shape[0])
        self._buildGraph()
        

    def _buildGraph(self):
        self.graphDim = self.numGridNodes.shape[0]
        self.totGraphNodes = np.prod(self.numGridNodes)
        self.graphLims = np.atleast_2d([[0,self.numGridNodes[i]-1] for i in xrange(self.graphDim)])
        self.graphElem = np.asarray(self.getGraphElem(self.graphDim, self.gridRadius, self.order, self.centralElem))
        self._gridMatrix = [np.linspace(self.gridLims[i,0], self.gridLims[i,1],self.numGridNodes[i],endpoint=True) for i in xrange(self.numGridNodes.shape[0])]
        
    @staticmethod
    def getGraphElem(graphDim, radius, order, centralElem):
        _a = list(itertools.product(np.arange(-radius, radius+1),repeat=graphDim))
        if centralElem:
            a = filter(lambda x: (np.linalg.norm(x,order) <= radius), _a)
        else:        
            a = filter(lambda x: (np.linalg.norm(x,order) <= radius) and not (np.array(x)==0).all(), _a)
        return a

    def graphNeighbors(self, graphX):
        graphX = np.atleast_2d(graphX)
        return filter(lambda x: (self.graphLims[:,0] <= x).all() and (x <= self.graphLims[:,1]).all(), np.asarray(graphX + self.graphElem))

    def gridNeighbors(self, gridX):
        return self.graphX2GridX(self.graphNeighbors(self.gridX2GraphX(gridX)))

    def gridX2GraphX(self, gridX):
        gridX = np.atleast_2d(gridX)
        return np.asarray([ [np.argmin(np.abs(gridX[i,j] - self._gridMatrix[j])) for j in xrange(gridX.shape[1])] for i in xrange(gridX.shape[0]) ])

    def graphX2GridX(self, graphX):
        graphX = np.atleast_2d(graphX)
        return np.asarray([ [self._gridMatrix[j][graphX[i,j]] for j in xrange(graphX.shape[1])] for i in xrange(graphX.shape[0]) ])
            
    def graphX2GraphIndx(self, graphX):
        graphX = np.atleast_2d(graphX)
        graphIndx = []
        for k in xrange(graphX.shape[0]):
            indx = 0
            for i in xrange(self.graphDim):
                indx = indx*self.numGridNodes[i] + graphX[k,i]
            graphIndx.append(indx)
        return np.array(graphIndx)

    def graphIndx2GraphX(self, graphIndx):
        graphIndx = np.atleast_1d(graphIndx).copy()
        graphX = []
        for k in xrange(graphIndx.shape[0]):
            X = np.zeros(self.graphDim, 'int')
            for i in xrange(self.graphDim):
                X[i] = graphIndx[k]/int(np.prod(self.numGridNodes[i+1:]))
                graphIndx[k] %= int(np.prod(self.numGridNodes[i+1:])) 
            graphX.append(X)
        return graphX


    def gridX2GraphIndx(self, gridX):
        return self.graphX2GraphIndx(self.gridX2GraphX(gridX))

    def graphIndx2GridX(self, graphIndx):
        return self.graphX2GridX(self.graphIndx2GraphX(graphIndx))

    def updateAdjMatrix(self, **kwargs):
        if kwargs.has_key('P') and kwargs.has_key('A'):
            self.A = kwargs['A']
            self.P = kwargs['P']
            return

        #complete graph
        def _completeGraph(A):
            A = np.ones([A.shape[0],A.shape[0]])
            return A

        #Grid Transition Types:
        def _stayswitch(A):
            assert(self.centralElem is True)
            trs = 2   #number of different transition types (actions)
            P = np.zeros([A.shape[0],trs,A.shape[0]])  # Transition Probability Model
            
            for i in xrange(A.shape[0]):
                nbrs = np.where(A[i])[0]
                for j in nbrs:
                    if len(nbrs)==1:
                        P[i,0,i] = 1
                        P[i,1,i] = 1
                    else:
                        if i==j:
                            P[i,0,i] = 1
                            P[i,1,i] = 0
                        else:
                            P[i,0,j] = 0
                            P[i,1,j] = 1/float(len(nbrs)-1)
            return P, trs

        self.mazeId = kwargs.get('mazeId', None)
        self.A = np.zeros([self.totGraphNodes,self.totGraphNodes])                          # Adjacency Matrix
        
        if self.mazeId is 'scomplete':
            self.A = _completeGraph(self.A)
            self.P, self.trs  = _stayswitch(self.A)
            
        elif self.mazeId is 'complete':
            self.A = _completeGraph(self.A)
            self.P = np.zeros([self.A.shape[0],self.A.shape[0],self.A.shape[0]])
            for s in xrange(self.A.shape[0]):
                self.P[s] = np.eye(self.A.shape[0])
        
        else:
            raise Exception('unrecognized mazetype')

        #nearest neighbors
        self.gN = []
        P = (self.P > 0).astype('int')
        for i in xrange(P.shape[0]):
            _an,_xn = np.where(P[i])
            self.gN.append([_xn, _an])

    
    def getTransProb(self, s, a, s_, inpType='graphIndx'):
        if not hasattr(self, 'P'):
            raise Exception('Call updateAdjacencyMatrix(mazeId=val, **kwargs) first!')
        if inpType is 'graphIndx':
            return self.P[s,a,s_]
        elif inpType is 'gridX':
            return self.P[self.gridX2GraphIndx(s),a,self.gridX2GraphIndx(s_)]
        elif inpType is 'graphX':
            return self.P[self.graphX2GraphIndx(s),a,self.graphX2GraphIndx(s_)]
        else:
            raise Exception('Invalid inpType. Valid types: graphIndx, gridX, graphX')

        
    def getValidNeighbors(self, s, inpType='graphIndx'):
        if not hasattr(self, 'gN'):
            raise Exception('Call updateAdjacencyMatrix(mazeId=val, **kwargs) first!')
        if inpType is 'graphIndx':
            return self.gN[s][0]
        elif inpType is 'gridX':
            return self.graphIndx2GridX(self.gN[self.gridX2GraphIndx(s)][0])
        elif inpType is 'graphX':
            return self.graphIndx2GraphX(self.gN[self.graphX2GraphIndx(s)][0])
        else:
            raise Exception('Invalid inpType. Valid types: graphIndx, gridX, graphX')

    def getValidTransitions(self, s, inpType='graphIndx'):
        if not hasattr(self, 'gN'):
            raise Exception('Call updateAdjacencyMatrix(mazeId=val, **kwargs) first!')
        if inpType is 'graphIndx':
            return self.gN[s][1]
        elif inpType is 'gridX':
            return self.gN[self.gridX2GraphIndx(s)][1]
        elif inpType is 'graphX':
            return self.gN[self.graphX2GraphIndx(s)][1]
        else:
            raise Exception('Invalid inpType. Valid types: graphIndx, gridX, graphX')
    
        
    def visualize(self, **kwargs):
        if (self.mazeId == 'complete') or (self.mazeId[1:] == 'complete'):
            print self.A
            print self.P
        else:
            print 'Nothing to visualize here!'

