 
__author__ = 'Varun Kompella, varun@idsia.ch'


import numpy as np
import time
try:
    from pyqtgraph.Qt import QtCore
    import pyqtgraph as pg
    PYQT = True
except ImportError:
    PYQT = False
from curiousDrMISFA import CuriousDrMISFANode
from misc import getWorkDir, getColors

import copy
import inputSignals

import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 


COLORSCHEME = 1         # 1 - white bg, 0 - black bg

###################################
#Experiment workspace
wrkdir = getWorkDir(1)

###################################
#Setup environment

class inputSrc(object):
    def __init__(self, configFile=None):
        self.userConfig = configFile
        self.t = 0
        self.count = 0
        self.stateLims = np.array([[0,self.userConfig.T-1]])

    def getObservation(self):
        obs = [np.atleast_2d(self.userConfig.signal()[self.t]), np.atleast_2d(self.t)]
        return obs
    
    def performAction(self):
        self.t += 1
        self.count += 1
        self.t %= self.userConfig.T
    
    def reset(self):
        self.t = 0
        self.count = 0

numSrcs = inputSignals.numSrcs

inputSrcs = []
for i in xrange(numSrcs):
    inputSrcs.append(inputSrc(configFile=inputSignals.signals[i]))

numModules = 3

###################################
#Adaptive module arguments

featureNos = [0]            
incsfaParams = {'input_dim': 2, 'whitening_output_dim' : 5, 'output_dim' : 1, 
                'eps' : 0.05, 'quadExp' : True,  'avgMode' : 'movAvg', 'avgN': 700}

rocParams = {'dim': inputSignals.totStates, 'n_clusters' : 2, 
             'trimvalue' : 0.2, 'amnesia' : 0.2}

for fno in featureNos: assert(fno<incsfaParams['output_dim'])
newExperiment = True
savedNetworkFile = None
adaptiveModuleArgs = [inputSrcs[0], featureNos, incsfaParams , rocParams, newExperiment, savedNetworkFile]

###################################
#Gating sys arguments

gatingThreshold = 0.3       # max(signal_tresh) (signal1: 0.81, signal1: 1.1, signal3: 2.0)
modulesDir = wrkdir
gatingSysArgs = [gatingThreshold, modulesDir]


###################################
#RL parameters

RLArgs = {'stateLims': inputSignals.GRIDLIMS, 'basis': 'Indicator', 
           'gamma': 0.99, 'args': { 'P': 'int', 'decoupled': False, 'mazeId': 'scomplete'}}


dRLArgs = {'numActions': inputSignals.numSrcs, 'stateLims': inputSignals.GRIDLIMS, 
           'basis': 'Indicator', 'gamma': 0.9, 
           'args': { 'P': 'int', 'decoupled': False, 'mazeId': 'complete'}}

#           'decay':0.99986,  #10 sigs (1st exp)
args = {'tau': 100, 'decay':0.998, 'delta': gatingThreshold, 'sigma': 50., 'beta':1.5, 'L': 200,
        'rlN' : 100,'RLArgs': RLArgs, 'eps':1.1, 'dRLArgs': dRLArgs, 'nu':1.2}

###################################

lastTime = time.time()
totalTime = time.time()

def testcuriousdrmisfa(animate=False):
 
    hist = {'intObservation':[], 'incSFAOutput': [], 'configData': [], 'rocEstError': [], 
            'rewardFunction1':[], 'intPolicy':[], 'intdPolicy':[], 
            'modLen':[], 'modSt':[], 'newEstLenThrsh': 0  }
     
    ##Setup node    
    curiousdrmisfanode = CuriousDrMISFANode(inputSrcs, args, adaptiveModuleArgs, gatingSysArgs)
     
 
    if PYQT and animate:
        global app
        app = pg.QtGui.QApplication([])
        
        if COLORSCHEME:
            bgcolor = 'w'
            fgcolor = 'k'
            bordercolor = (200,200,200)
            
        else:
            bgcolor = 'k'
            fgcolor = 'w'
            bordercolor = (50,50,50)
            
         
        pg.setConfigOption('background', bgcolor)
        pg.setConfigOption('foreground', fgcolor)
        pg.setConfigOptions(antialias=True)
 
        ##Setup Plot
 
        view = pg.GraphicsView()
        l = pg.GraphicsLayout(border=bordercolor)
        view.setCentralItem(l)
        view.setWindowTitle('Curious Dr. MISFA Test')
        view.resize(900,800)
 
        ## Title at top
         
        text = """ This example demonstrates the working of Curious Dr. MISFA. """
        l.addLabel(text, col=0, colspan=3, size='20px')
        l.nextRow()
  
        pen = pg.mkPen(color=fgcolor, width=1)
        labelStyle = {'color': '#FFF', 'font-size': '16px'}
        
        l1 = l.addLayout(col=0, colspan=3, border=bordercolor)
        l1.setContentsMargins(10, 10, 10, 10)

        #Agent's Internal State
        p11 = l1.addPlot() 
        spots11 = pg.ScatterPlotItem(size=70, pen=pg.mkPen(None), brush=pg.mkBrush(150, 150, 150))
        pos = np.array([[1,1.732],[2,0],[0,0]])
        spots = [{'pos': pos[i,:], 'data': 1} for i in xrange(pos.shape[0])]
        spots11.addPoints(spots)
        p11.addItem(spots11)
        p11.setXRange(-1,3); p11.setYRange(-1,2.5)
        p11.setTitle('Agent\'s Internal State', size='16px')
        p11.hideAxis('left'); p11.hideAxis('bottom')
        texts = [pg.TextItem(html='<div style="text-align: center"><span style="color: #000; font-size: 20pt; font-weight: bold">s<sub>%d</sub></span></div>'%(i+1)) for i in xrange(numSrcs)]
        textints = [pg.TextItem(html='<div style="text-align: center"><span style="color: #000; font-size: 12pt;"><i>int</i></span></div>') for _ in xrange(numSrcs)]
        [p11.addItem(text) for text in texts]
        [p11.addItem(text) for text in textints]
        offset = np.array([-0.18,0.32])
        intoffset = np.array([0.01,.4])
        [text.setPos(*list(pos[i] + offset)) for i,text in enumerate(texts)]
        [text.setPos(*list(pos[i] + intoffset)) for i,text in enumerate(textints)]

        #Output plot
        curves12 = [pg.PlotCurveItem(pen=pg.mkPen(color=getColors(incsfaParams['output_dim'])[i],width=1.)) for i in range(incsfaParams['output_dim'])]
        p12 = l1.addPlot(); 
        for c in curves12: p12.addItem(c)
        p12.setContentsMargins(5, 5, 5, 5)
        p12.getAxis('left').setPen(pen)
        p12.getAxis('bottom').setPen(pen)
        p12.setLabel('bottom', 's'+u'\u2020', **labelStyle)
        p12.setTitle('IncSFA Output', size='16px')
        p12.setXRange(0,inputSignals.totStates); p12.setYRange(-2,2)

        #Config data plot
        curves13 = [pg.PlotCurveItem(pen=pg.mkPen(color=getColors(inputSrcs[0].stateLims.shape[0])[i],width=1.2)) for i in range(inputSrcs[0].stateLims.shape[0])]
        p13 = l1.addPlot();  
        for c in curves13: p13.addItem(c)
        p13.setContentsMargins(5, 5, 5, 5)
        p13.getAxis('left').setPen(pen)
        p13.getAxis('bottom').setPen(pen)
        p13.setLabel('bottom', 's'+u'\u2020', **labelStyle)
        p13.setTitle('Config Data', size='16px')
        p13.setXRange(0,inputSignals.totStates); p13.setYRange(0,inputSignals.totStates)
 
        l1.nextRow()
 
        #IR plot
        curves21 = pg.PlotCurveItem(pen=pg.mkPen(color=fgcolor,width=1.2))
        p21 = l1.addPlot(); p21.addItem(curves21) 
        p21.setContentsMargins(5, 5, 5, 5)
        p21.getAxis('left').setPen(pen)
        p21.getAxis('bottom').setPen(pen)
        p21.setLabel('bottom', 't', units=' x %d'%(args['tau']), **labelStyle)
        p21.setTitle('Intrinsic Reward', size='16px')
        p21.setYRange(-200,200)
 
        #ROC error plot    
        curves22 = [pg.PlotCurveItem(pen=pg.mkPen(color=getColors(numModules)[i],width=1.2)) for i in range(numModules)]
        p22 = l1.addPlot();  
        for c in curves22: p22.addItem(c)
        p22.setContentsMargins(5, 5, 5, 5)
        p22.getAxis('left').setPen(pen)
        p22.getAxis('bottom').setPen(pen)
        p22.setLabel('bottom', 't', **labelStyle)
        p22.setTitle('ROC Error', size='16px')
 
        #ROC Clusters plot
        curves23 = [pg.PlotCurveItem(pen=pg.mkPen(color=getColors(incsfaParams['output_dim'])[i],width=1.)) for i in range(incsfaParams['output_dim'])]
        p23 = l1.addPlot(); 
        for c in curves23: p23.addItem(c)
        p23.setContentsMargins(5, 5, 5, 5)
        p23.getAxis('left').setPen(pen)
        p23.getAxis('bottom').setPen(pen)
        ROCClusPlotUpdateFreq = 30
        p23.setLabel('bottom', 's'+u'\u2020  (plot update 1:%dt)'%(ROCClusPlotUpdateFreq*args['tau']), **labelStyle)
        p23.setTitle('ROC Clusters', size='16px')
        p23.setXRange(0,rocParams['dim']); p23.setYRange(-2,2)
 
        l1.nextRow()
         
        #Reward Function plot
        curves311 = [pg.PlotCurveItem(pen=pg.mkPen(color=getColors(numSrcs)[i],width=1.)) for i in range(numSrcs)]
        curves312 = [pg.PlotCurveItem(pen=pg.mkPen(color=getColors(numSrcs)[i],width=1.,style=QtCore.Qt.DashLine)) for i in range(numSrcs)]
        p31 = l1.addPlot(colspan=2); 
        for c in curves311: p31.addItem(c)
        for c in curves312: p31.addItem(c)
        p31.setContentsMargins(5, 5, 5, 5)
        p31.getAxis('left').setPen(pen)
        p31.getAxis('bottom').setPen(pen)
        p31.setLabel('bottom', 't', units=' x %d'%(args['tau']), **labelStyle)
        p31.setTitle('Reward Function', size='16px')
        p31.setYRange(-1.5,1.5)
 
        #Policy plot
        curves32 = [pg.PlotCurveItem(pen=pg.mkPen(color=getColors(numSrcs)[i],width=1.)) for i in range(numSrcs)]
        p32 = l1.addPlot(); 
        for c in curves32: p32.addItem(c)
        p32.setContentsMargins(5, 5, 5, 5)
        p32.getAxis('left').setPen(pen)
        p32.getAxis('bottom').setPen(pen)
        p32.setLabel('bottom', 't', units=' x %d'%(args['tau']), **labelStyle)
        p32.setTitle('Policy', size='16px')
        p32.setYRange(3-3*(numSrcs-1)-0.1,4.1)
         

        view.show()
        timer = QtCore.QTimer()
        aloop = lambda : animloop(hist)
        def animloop(hist):
            global lastTime, totalTime
            curiousdrmisfanode.update()
            hist = curiousdrmisfanode.monitorVariables(hist)

            ######## plot updates ########
              
#                 #Agent's Internal State
            intObservation = np.asarray(hist['intObservation'])
            if intObservation.shape[0]>1:
                spots11.points()[int(intObservation[-2,0])].resetPen()
            spots11.points()[int(intObservation[-1,0])].setPen('#00A',width=10)

 
            #Output plot
            hist['incSFAOutput'] = hist['incSFAOutput'][-inputSignals.totStates/args['tau']:]
            for i in range(incsfaParams['output_dim']):
                curves12[i].setData(np.atleast_2d(hist['incSFAOutput'])[:,:,i].ravel())
 
            #Config data plot
            hist['configData'] = hist['configData'][-inputSignals.totStates/args['tau']:]
            for i in range(inputSrcs[0].stateLims.shape[0]):
                curves13[i].setData(np.atleast_2d(hist['configData'])[:,:,i].ravel())
 
            #IR plot
            curves21.setData(x=np.arange(max(0,intObservation.shape[0]-100),intObservation.shape[0]), y=intObservation[-100:,4])

            #ROC error plot    
            a = copy.deepcopy(hist['modLen'])
            a.append(len(hist['rocEstError']))
            a.insert(0,0)
            for i in xrange(len(a)-1):
                curves22[i].setData(x=np.arange(a[i], a[i+1])[::args['tau']], y=hist['rocEstError'][a[i]:a[i+1]][::args['tau']])
 
    
            #ROC Clusters plot
            if curiousdrmisfanode.cnt % ROCClusPlotUpdateFreq == 0:
                [xc,yc] = curiousdrmisfanode.adapModule.getEstMap()
                if (len(xc)> 0) and (len(yc)>0):
                    for i,fno in enumerate(featureNos):
                        curves23[fno].setData(x=np.ravel(xc), y=np.asarray(yc)[:,i])
   
            #Reward Function plot
            RFUNC = np.asarray(hist['rewardFunction1'])
            for i in xrange(numSrcs):
                curves311[i].setData(RFUNC[:,i,0,i])
                if numSrcs > 1:
                    curves312[i].setData(np.sum(RFUNC[:,i,1,:],1)/(numSrcs-1.))
                else:
                    curves312[i].setData(RFUNC[:,i,1,i])
   
            #Policy plot
            x = np.asarray(hist['intPolicy'])
            for i in xrange(numSrcs):
                curves32[i].setData(x[:,i]+3-3*i)


            print 'ROC Est Error: ', hist['rocEstError'][-1]
            print 'Eps: ', curiousdrmisfanode.eps, curiousdrmisfanode.nu, curiousdrmisfanode.dtCnt
            print 'Internal policy: ', hist['intPolicy'][-1]
            print 'Internal dPolicy: ', hist['intdPolicy'][-1]
            now = time.time(); dt = now - lastTime; lastTime = now
            print 'Number of updates: ', curiousdrmisfanode.cnt, '.  Time per iter %.4f'%(dt) 

            estErrorHist = hist['rocEstError']
            if len(estErrorHist) > hist['newEstLenThrsh'] + 3000:
                if np.max(estErrorHist[-inputSignals.totStates:]) < gatingThreshold:
                    hist['newEstLenThrsh'] = len(estErrorHist)
                    hist['modLen'].append(len(estErrorHist))
                    hist['modSt'].append(hist['intObservation'][-1][0])
                    curiousdrmisfanode.saveCurrModule()
                    curiousdrmisfanode.createNewModule()
           
            if (len(hist['modLen']) == numModules):
                _t = time.time()
                print '\nNumber of updates: %d'%(curiousdrmisfanode.cnt)
                print '\nTotal time taken %.4f'%(_t-totalTime)
                print '\nAvg. time per iteration %.4f'%((_t-totalTime)/curiousdrmisfanode.cnt) 
                curves21.setData(intObservation[:,4])
                return
               
            aloop = lambda : animloop(hist)
            timer.singleShot(0,aloop)

        timer.singleShot(0,aloop)
        app.exec_()
    else:
        from matplotlib import pyplot as plt
        while(1):
            global lastTime
            curiousdrmisfanode.update()
            hist = curiousdrmisfanode.monitorVariables(hist)
            
            print 'ROC Est Error: ', hist['rocEstError'][-1]
            print 'Eps: ', curiousdrmisfanode.eps, curiousdrmisfanode.nu, curiousdrmisfanode.dtCnt
            print 'Internal policy: ', hist['intPolicy'][-1]
            print 'Internal dPolicy: ', hist['intdPolicy'][-1]
            now = time.time(); dt = now - lastTime; lastTime = now
            print 'Number of updates: ', curiousdrmisfanode.cnt, '.  Time per iter %.4f'%(dt) 
            
            estErrorHist = hist['rocEstError']
            if len(estErrorHist) > hist['newEstLenThrsh'] + 3000:
                if np.max(estErrorHist[-inputSignals.totStates:]) < gatingThreshold:
                    hist['newEstLenThrsh'] = len(estErrorHist)
                    hist['modLen'].append(len(estErrorHist))
                    hist['modSt'].append(hist['intObservation'][-1][0])
                    curiousdrmisfanode.saveCurrModule()
                    curiousdrmisfanode.createNewModule()
            
            if (len(hist['modLen']) == numModules):
                _t = time.time()
                print '\nNumber of updates: %d'%(curiousdrmisfanode.cnt)
                print '\nTotal time taken %.4f'%(_t-totalTime)
                print '\nAvg. time per iteration %.4f'%((_t-totalTime)/curiousdrmisfanode.cnt) 
                
                fig = plt.figure(figsize=(10, 8))
                fig.canvas.set_window_title('This example demonstrates the working of Curious Dr. MISFA.')
                
                ax = [plt.subplot2grid((3,3), (0,0)), plt.subplot2grid((3,3), (0,1)), plt.subplot2grid((3,3), (0,2))]
                ax.extend([plt.subplot2grid((3,3), (1,0)), plt.subplot2grid((3,3), (1,1)), plt.subplot2grid((3,3), (1,2))])
                ax.extend([plt.subplot2grid((3,3), (2,0), colspan=2), plt.subplot2grid((3,3), (2,2))])
                
                #Input streams plot
                intObservation = np.asarray(hist['intObservation'])
                for i in range(incsfaParams['input_dim']):
                    ax[0].plot(inputSignals.signals[int(intObservation[-1,0])].signal()[:,i], c=getColors(incsfaParams['input_dim'])[i])
                ax[0].set_title('Selected Input Stream', fontsize=16)
                ax[0].set_xlabel('s'+u'\u2020')
               
                #Output plot
                hist['incSFAOutput'] = hist['incSFAOutput'][-inputSignals.totStates/args['tau']:]
                for i in range(incsfaParams['output_dim']):
                    ax[1].plot(np.atleast_2d(hist['incSFAOutput'])[:,:,i].ravel(), c=getColors(incsfaParams['output_dim'])[i])
                ax[1].set_title('IncSFA Output', fontsize=16)
                ax[1].set_xlabel('s'+u'\u2020')

                #Config data plot
                hist['configData'] = hist['configData'][-inputSignals.totStates/args['tau']:]
                for i in range(inputSrcs[0].stateLims.shape[0]):
                    ax[2].plot(np.atleast_2d(hist['configData'])[:,:,i].ravel(), c=getColors(inputSrcs[0].stateLims.shape[0])[i])
                ax[2].set_title('Config Data', fontsize=16)
                ax[2].set_xlabel('s'+u'\u2020')

                #IR plot
                ax[3].plot(intObservation[:,4])
                ax[3].set_title('Intrinsic Reward', fontsize=16)
                ax[3].set_xlabel('t' + ' x %d'%(args['tau']))

                #ROC error plot    
                a = copy.deepcopy(hist['modLen'])
                a.insert(0,0)
                for i in xrange(len(a)-1):
                    ax[4].plot(np.arange(a[i], a[i+1]),hist['rocEstError'][a[i]:a[i+1]],c=getColors(numModules)[i])
                ax[4].set_title('ROC Error')
                ax[4].set_xlabel('t')
                
                #ROC Clusters plot
                [xc,yc] = curiousdrmisfanode.gatingSys.trainedModules[-1][1].getEstMap()
                for i,fno in enumerate(featureNos):
                    ax[5].plot(np.ravel(xc),np.asarray(yc)[:,i], c=getColors(incsfaParams['output_dim'])[i])
                ax[5].set_ylim([-2.0,2.0])
                ax[5].set_title('ROC Clusters', fontsize=16)
                ax[5].set_xlabel('s'+u'\u2020')
                
                #Reward Function plot
                RFUNC = np.asarray(hist['rewardFunction1'])
                for i in xrange(numSrcs):
                    ax[6].plot(RFUNC[:,i,0,i],c=getColors(numSrcs)[i])
                    if numSrcs > 1:
                        ax[6].plot(np.sum(RFUNC[:,i,1,:],1)/(numSrcs-1.),c=getColors(numSrcs)[i], linestyle='--')
                    else:
                        ax[6].plot(RFUNC[:,i,1,i],c=getColors(numSrcs)[i], linestyle='--')
                ax[6].set_ylim([-1,1])
                ax[6].set_title('Reward Function', fontsize=16)
                ax[6].set_xlabel('t' + ' x %d'%(args['tau']))
                
                #Policy plot
                x = np.asarray(hist['intPolicy'])
                for i in xrange(numSrcs):
                    ax[7].plot(x[:,i]+3-3*i, c=getColors(numSrcs)[i])
                ax[7].set_xlabel('t' + ' x %d'%(args['tau']))
                ax[7].set_title('Policy', fontsize=16)
                ax[7].set_ylim([3-3*(numSrcs-1)-0.1,4.1])
          
                plt.tight_layout()
                plt.show()
                
                break
     
#######################################################################################
#######################################################################################
 
if __name__ == "__main__":
    testcuriousdrmisfa(animate=1)
    



