 
================================================================================
Curiosity Driven Modular Incremental Slow Feature Analysis (Curious Dr. MISFA)
================================================================================

Author - Varun Raj Kompella. (https://varunrajk.gitlab.io)

This is a free software; you can redistribute it and/or modify it. 
The code is distributed in the hope that it will be useful.  

If you plan to use this code in your research
please cite either of these, whichever is appropriate:


V. R. Kompella, M. Luciw, M. Stollenga and J. Schmidhuber. "Optimal Curiosity Driven Modular Incremental Slow Feature Analysis", Accepted for Neural Computation Journal, 2016.

M. Luciw*, V. R. Kompella*, S. Kazerounian and J. Schmidhuber. "An intrinsic value system for developing multiple invariant representations with incremental slowness learning", Frontiers in Neurorobotics, Vol. 7 (9), 2013. *Joint first authors.

V. R. Kompella, M. Luciw, M. Stollenga, L. Pape and J. Schmidhuber. "Autonomous Learning of Abstractions using Curiosity-Driven Modular Incremental Slow Feature Analysis. (Curious Dr. MISFA)" , IEEE International Conference on Developmental and Learning and Epigenetic Robotics (ICDL-EpiRob), San Diego, 2012.



Abstract 
--------

Curious Dr. MISFA actively learns multiple expert modules comprising slow feature abstractions in the order of increasing learning difficulty.


Dependencies
---------------------

- PyQTGraph for fast animated plotting




