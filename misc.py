

import numpy as np


##############################################################################################
#### Save and load pickled files
##############################################################################################
import cPickle

def savefile(filename, data):
    fh = open(filename, 'wb')
    cPickle.dump(data, fh)
    fh.close()
  
def loadfile(filename):
    fh = open(filename, 'r')
    out = cPickle.load(fh)
    fh.close()
    return out

##############################################################################################
#### RouletteWheel for random selection
##############################################################################################

def rouletteWheel(probVec):
    rndNr = np.random.rand()
    pi=0.;pj=0.;indx = None
    for i in xrange(len(probVec)):
        pj  = probVec[i]
        if (pi<=rndNr) and (rndNr<(pi+pj)):
            indx = i
            break
        else:
            pi+=pj
    return indx

##############################################################################################
#### Working directory to save algorithm outputs 
##############################################################################################
import os

def getWorkDir(expid=None):
    if expid is None:
        expid = 0
        while(1):
            if not (os.path.exists('/tmp/varun/')):
                os.mkdir('/tmp/varun/')
            workDir = '/tmp/varun/' + str(expid) + '/'
            if (os.path.exists(workDir)):
                expid+=1
            else:
                os.mkdir(workDir)
                print 'The expid for this experiment is: ', expid
                break
    else:
        workDir = '/tmp/varun/' + str(expid) + '/'
        if not (os.path.exists(workDir)):
            if not (os.path.exists('/tmp/varun/')):
                os.mkdir('/tmp/varun/')
            os.mkdir(workDir)
    print 'The expid for this experiment is: ', expid
    return workDir


##############################################################################################
#### Color values for plotting
##############################################################################################
import colorsys

def getColors(num_colors):
    colors=[]
    for i in np.arange(0., 360., 360. / num_colors):
        hue = i/360.
        lightness = (50 + np.random.rand() * 10)/100.
        saturation = (90 + np.random.rand() * 10)/100.
        rgb = np.round(np.asarray(colorsys.hls_to_rgb(hue, lightness, saturation))*255)
        rgb = np.clip(rgb, 0, 255)
        hexc = '#%02x%02x%02x' %(rgb[0], rgb[1], rgb[2])
        colors.append(hexc)
    return colors


##############################################################################################
#### IncSFA Curiosity Function (Omega)
##############################################################################################
from mdp.nodes import WhiteningNode, PCANode, PolynomialExpansionNode

class omegaNode(object):
    def __init__(self, eta, degree=1):
        self.eta = eta
        self.expnode = PolynomialExpansionNode(degree)

    def getOmega(self, X, eta=None):
        Omgs = []
        if type(X) is not list:
            X = [X]
        for i in xrange(len(X)):
            x = self.expnode(X[i])
            if eta is None:
                eta = self.eta
            wnode = WhiteningNode()
            pnode = PCANode()
            wnode(x)
            xdot = x[1:, :]-x[:-1, :]
            zdot = wnode(xdot)
            pnode(zdot)
            L = pnode.d
            Omgs.append((1. - (eta*(L[-2]-L[-1])/(1.5 - eta - eta*L[-1]))))
        return Omgs

    def signalOmegaCheck(self, X, gamma):
        if type(X) is not list:
            X = [X]
        n = len(X)
        r = gamma/((n-1) - gamma*(n-2))
        Omgs = self.getOmega(X)
        Omgss, Omgsi = (np.sort(Omgs), np.argsort(Omgs))
        allcheck = True
        for i in xrange(n):
            for j in xrange(i+1,n):
                if np.log(Omgss[j])/np.log(Omgss[i]) > r:
                    print 'signals %d and %d violate the condition'%(Omgsi[i]+1,Omgsi[j]+1)
                    allcheck = False
        if allcheck:
            print 'Signals OK!'
