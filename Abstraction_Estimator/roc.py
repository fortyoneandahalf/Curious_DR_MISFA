
__author__ = 'Varun Kompella, varun@idsia.ch'


"""
kernal based ROC Agglomerative Clustering node

"""

import math
import heapq
import operator
import numpy as np
import scipy
from scipy.cluster.vq import kmeans2


def kernel_linear(x,y):
    return scipy.dot(x,y)

def kernel_poly(x,y,a=1.0,b=1.0,p=2.0):
    return (a*scipy.dot(x,y)+b)**p

#def kernel_gauss(x,y, sigma=0.00001):
def kernel_gauss(x,y, sigma=0.2):
    v=x-y
    l=math.sqrt(scipy.square(v).sum())
    return math.exp(-sigma*(l**2))

def kernel_normalise(k): 
    return lambda x,y: k(x,y)/math.sqrt(k(x,x)+k(y,y))

kernel=kernel_normalise(kernel_gauss)

def kernel_dist(x,y):
    # if gaussian kernel:
    return 2-2*kernel(x,y)
    # if not
    #return kernel(x,x)-2*kernel(x,y)+kernel(y,y)

class Cluster(object):
    def __init__(self, a): 
        self.center=a
        self.size=1.
        self.count=0
        
    def add(self, e):
        self.count+=1
        self.size+=kernel(self.center, e)
        self.center+=(e-self.center)/self.size

    def merge(self, c):
        self.center=(self.center*self.size+c.center*c.size)/(self.size+c.size)
        self.size+=c.size

    def leak(self,amnesia=0.):
        self.size -= (self.size - 1) * amnesia
#        self.size = np.clip(self.size,1,30)
        #print self.size

    def resize(self,dim):
        extra=scipy.zeros(dim-len(self.center))
        self.center=scipy.append(self.center, extra)
        
    def __str__(self):
        return "Cluster( %s, %f )"%(self.center, self.size)

class Dist(object):
    """this is just a tuple,
    but we need an object so we can define cmp for heapq"""
    def __init__(self,x,y,d):
        self.x=x
        self.y=y
        self.d=d
    def __cmp__(self,o):
        return cmp(self.d,o.d)
    def __str__(self):
        return "Dist(%f)"%(self.d)

class OnlineCluster(object) : 
    
    def __init__(self, N, trimvalue = .05, amnesia = 0.0):
        """N-1 is the largest number of clusters I can find
        Higher N makes me slower"""
        
        self.n=0
        self.N=N
                
        self.clusters=[]
        # max number of dimensions we've seen so far
        self.dim=0 
        self.error = 0

        # cache inter-cluster distances
        self.dist=[]
        self.trimvalue = trimvalue
        self.amnesia = amnesia

    def resize(self, dim):
        for c in self.clusters:
            c.resize(dim)
        self.dim=dim

    def removedist(self,c):
        """invalidate intercluster distance cache for c"""
        r=[]
        for x in self.dist:
            if x.x==c or x.y==c: 
                r.append(x)
        for x in r: self.dist.remove(x)
        heapq.heapify(self.dist)
        
    def updatedist(self, c):
        """Cluster c has changed, re-compute all intercluster distances"""
        self.removedist(c)

        for x in self.clusters:
            if x==c: continue
            d=kernel_dist(x.center,c.center)
            t=Dist(x,c,d)
            heapq.heappush(self.dist,t)
                
    def trimclusters(self):
        """Return only clusters over threshold"""
        t=scipy.mean([x.size for x in filter(lambda x: x.size>0, self.clusters)]) * self.trimvalue
#         try:
#             t=np.max([x.size for x in filter(lambda x: x.size>0, self.clusters)]) * self.trimvalue
#         except ValueError:
#             t=scipy.mean([x.size for x in filter(lambda x: x.size>0, self.clusters)]) * self.trimvalue
            
        return filter(lambda x: x.size>=t, self.clusters)

    
    def update(self, e):
        if len(e)>self.dim:
            self.resize(len(e))

        if len(self.clusters)>0: 
            # compare new points to each existing cluster
            c=[ ( i, kernel_dist(x.center, e) ) for i,x in enumerate(self.clusters)]
            closest=self.clusters[min( c , key=operator.itemgetter(1))[0]]
            
#            self.error = np.abs(e - closest.center)
#            self.error = self.geterror(e)
            
            closest.add(e)
            # invalidate dist-cache for this cluster
            self.updatedist(closest)
            
        if len(self.clusters)>=self.N and len(self.clusters)>1:
            # merge closest two clusters
            m=heapq.heappop(self.dist)
            m.x.merge(m.y)
            
            self.clusters.remove(m.y)
            self.removedist(m.y)
            
            self.updatedist(m.x)

        ##amnesia for clusters
        [c.leak(amnesia=self.amnesia) for c in self.clusters]

#        #ugly hack
#        for c in self.clusters:
#            amn = self.amnesia*np.power(np.e,-.2*(30.-c.size))            
#            c.leak(amnesia=amn)

#        print [c.size for c in self.clusters]
                
        # make a new cluster for this point
        newc=Cluster(e)
        self.clusters.append(newc)
        self.updatedist(newc)
        
        self.n+=1

    def geterror(self, e=None):
        if e is not None:
            # compare new point to each existing cluster
            if len(self.clusters)>0: 
                clusters = self.trimclusters()
                c=[ ( i, kernel_dist(x.center, e) ) for i,x in enumerate(clusters)]
                closest=clusters[min( c , key=operator.itemgetter(1))[0]]
                error = np.sum(np.abs(e - closest.center))
#                error = np.min(np.abs(e - closest.center))
#                error = np.linalg.norm(e - closest.center)
            else:
                error = 1.0
            return error
        else:
            print 'observation not provided'
            return 0.0


class ROCClusteringNode(object):
    def __init__(self, dim, n_clusters=5, trimvalue=0.1, amnesia=0.):
        self.dim = np.atleast_1d(dim)      # dim = [dim1, dim2, ..., dimn]
        self.totDim = np.product(self.dim)
        self.n_clusters = n_clusters
        self.trimvalue = trimvalue
        self.amnesia = amnesia
        self._init_node()
        self.nanFlag = False

    def _init_node(self):
        self.clusters = [OnlineCluster(self.n_clusters, self.trimvalue, self.amnesia) for _ in xrange(self.totDim)]
        self.trimmedClusters = [None]*self.totDim
        self.estimationError = np.zeros(self.totDim,'float32')
        self.dEstimationError = np.zeros(self.totDim,'float32')
        self.dEstError = 0.0
        self._cnt = 0

        self.idCluster = OnlineCluster(self.n_clusters, 0.0, self.amnesia)

    def update(self, obs, state):
        if np.isnan(obs).any():
            self.nanFlag = True
            return 0
        else:
            self.nanFlag = False
        assert (len(state) == len(self.dim))
        st = 0
        for i in xrange(len(state)):
            st += int(state[i]*np.product(self.dim[(i+1):]))

        if self._cnt >= self.totDim:
            prevEstError = self.estimationError[st]

        #compute the estimation error        
        self.estimationError[st] = self.clusters[st].geterror(np.atleast_1d(obs))

        #update the estimator with the new observation      
        self.clusters[st].update(np.atleast_1d(obs))
        
        if self._cnt >= 2*self.totDim:
            #compute the decrease in estimation error        
            self.dEstError = (self.estimationError[st] - prevEstError)

        self._cnt+=1

        
    def getEstimationError(self, x=None, state=None):
        if x is None:
            return self.estimationError.sum()
        else:
            state = np.atleast_2d(state)
            assert (state.shape[1] == len(self.dim))
            tot_err = 0.0
            for i in xrange(len(x)):
                st = 0
                for j in xrange(state.shape[1]):
                    st += int(state[i][j]*np.product(self.dim[(j+1):]))
                tot_err += self.clusters[st].geterror(x[i])
            return tot_err
            
    def getDerivEstimationError(self):
        if self.nanFlag:
            return 0.0
        else:
            return self.dEstError

    def getEstMap(self):
        self.clusIndx = [{} for _ in xrange(len(self.clusters))]
        x_cluster_list, y_cluster_list = [], []
        indx = 0
        for i, c in enumerate(self.clusters):
            self.trimmedClusters[i] = c.trimclusters()
            for cindx, center_cluster in enumerate(self.trimmedClusters[i]):#c.clusters:
                k=i; x = np.zeros(len(self.dim))
                for j in xrange(x.shape[0]):
                    x[j] = int(k/np.product(self.dim[(j+1):]))
                    k-= x[j]*np.product(self.dim[(j+1):])  
                x_cluster_list.append(x)
                y_cluster_list.append(center_cluster.center)
                self.clusIndx[i][cindx] = indx
                indx+=1
        return [x_cluster_list, y_cluster_list]


    def getEstimation(self, obs, state):
        obs = np.atleast_1d(obs)
        assert (len(state) == len(self.dim))
        st = 0
        for i in xrange(len(state)):
            st += int(state[i]*np.product(self.dim[(i+1):]))

        c=[(i, kernel_dist(x.center, obs)) for i,x in enumerate(self.trimmedClusters[st])]
        cindx = min( c , key=operator.itemgetter(1))[0]
        closest=self.trimmedClusters[st][cindx]
        
        return closest.center, self.clusIndx[st][cindx] 

    def clusterIDs(self, clus=None):
        if clus is None:
            n_clusters = np.max([len(self.trimmedClusters[i]) for i in xrange(self.totDim)])
        else:
            n_clusters= clus
        self.clusCenters = np.asarray([x.center for i in xrange(self.totDim) for x in self.trimmedClusters[i]])
        self.clusID = kmeans2(self.clusCenters,n_clusters)[1]
        
    def getClusID(self, obs, state):
        if not hasattr(self, 'clusID'):
            self.clusterIDs()
#         return self.clusID[np.argmin(np.abs(self.getEstimation(obs, state)[0]-self.clusCenters))]
        return self.clusID[self.getEstimation(obs, state)[1]]
        

